# Plasma Mobile Website

This repository contains the files needed for building https://plasma-mobile.org

## Building

The website can be built and served locally after installing the dependencies.
```sh
bundle install
```
To build the website and start the development server, use
```sh
bundle exec jekyll serve
```

## Maintainance

Please only update the ruby gems using the `updategems.sh` script in the root of the repository.
We need the Gemfile.lock to work with different versions of bundler, and the normal update process locks the file to a specific one.
