---
layout: page
title: Our Vision
permalink: /vision/
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
menu:
  main:
    parent: "Project"
    weight: 1
    name: Vision
---

Plasma Mobile aims to become a complete and open software system for mobile devices.<br /> It is designed to give privacy-aware users back control over their information and communication.

Plasma Mobile takes a pragmatic approach and is inclusive to 3rd party software, allowing the user to choose which applications and services to use, while providing a seamless experience across multiple devices.<br /> Plasma Mobile implements open standards and is developed in a transparent process that is open for anyone to participate in.
