---
title: Screenshots
permalink: /screenshots/
layout: page

screenshots:
  - url: plasma.png
    name: "Plasma mobile homescreen"
  - url: weather.png
    name: KWeather, Plasma mobile weather application
  - url: pp_calculator.png
    name: Kalk, a calculator application
  - url: pp_camera.png
    name: Megapixels, a camera application
  - url: pp_calindori.png
    name: Calindori, a calendar application
  - url: pp_kclock.png
    name: KClock
  - url: pp_buho.png
    name: Buho, a note taking application
  - url: pp_kongress.png
    name: Kongress
  - url: pp_okular01.png
    name: Okular Mobile, a universal document viewer
  - url: pp_angelfish.png
    name: Angelfish, a web browser
  - url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
    name: Nota, a text editor
  - url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
    name: Pix, another image viewer
  - url: pp_folders.png
    name: Index, a file manager
  - url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
    name: VVave, a music player
  - url: 20201110_092718.jpg
    name: The hardware

menu:
  main:
    parent: "Project"
    weight: 2
sassFiles:
  - scss/screenshots.scss
  - scss/components/swiper.scss
---

The following screenshots were taken from a Pinephone device running Plasma Mobile.

{{< screenshots name="screenshots" >}}

<script src="https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js"></script>
